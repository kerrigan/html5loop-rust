use std::fs;
use yarte::TemplateText;

#[derive(TemplateText)]
#[template(path = "template.hbs")]
struct StateTemplate {
    video_str: String,
    audio_str: String,
}

fn main() -> Result<(), ()> {
    let audio_str = read_audio()?;
    let video_str = read_video()?;
    let template = StateTemplate {
        video_str,
        audio_str
    };
    let result = template.call().unwrap();
    fs::write("output.html", result).map_err(|e|{
        eprintln!("{}", e);
        ()
    })?;
    Ok(())
}


fn read_audio() -> Result<String, ()> {
    let contents = fs::read("audio.mp3").map_err(|e|{
        eprintln!("{}", e);
        ()
    })?;
    Ok(base64::encode(contents))
}

fn read_video() -> Result<String, ()> {
    let contents = fs::read("video.webm").map_err(|e|{
        eprintln!("{}", e);
        ()
    })?;
    Ok(base64::encode(contents))
}